$(document).ready(function(){
    $('.first-screen .middle').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        fade: true,
        speed: 1000
    });

    //menu slideUp/Down
    $('.first-screen .top .second nav ul li.parent').hover(
        function(){
            $('ul:first', this).slideDown(400);
        },
        function(){
            $('ul:first', this).slideUp(400);
        }
    );

    // Add smooth scrolling to all links
    $("a").on('click', function(event) {
        event.preventDefault();
        if (this.hash !== "") {
            $('#offcanvas-menu').removeClass('active');
            $('body').css('overflow-y', 'auto');

            $('.accordion > p.parent').each(function(){
                $(this).next().slideUp();
                $(this).find('i').removeClass('transform');
            });

            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function(){
                window.location.hash = hash;
            });
        }
    });

    //add classes on How-We-Work blocks hover
    $('.howWeWork .block.first').hover(
        function() {
            $( this ).next().addClass('hide-top-left');
        }, function() {
            $( this ).next().removeClass('hide-top-left');
        }
    );
    $('.howWeWork .block.second').hover(
        function() {
            $( this ).prev().addClass('hide-bottom-right');
            $( this ).next().addClass('hide-bottom-left');
        }, function() {
            $( this ).prev().removeClass('hide-bottom-right');
            $( this ).next().removeClass('hide-bottom-left');
        }
    );
    $('.howWeWork .block.third').hover(
        function() {
            $( this ).prev().addClass('hide-top-right');
            $( this ).next().addClass('hide-top-left');
        }, function() {
            $( this ).prev().removeClass('hide-top-right');
            $( this ).next().removeClass('hide-top-left');
        }
    );
    $('.howWeWork .block.forth').hover(
        function() {
            $( this ).prev().addClass('hide-bottom-right');
        }, function() {
            $( this ).prev().removeClass('hide-bottom-right');
        }
    );

    if($('.first-screen .second nav .mobile').is(':visible')){
        $('.howWeWork .block.second').hover(
            function() {
                $( this ).next().addClass('hide-top-left');
            }, function() {
                $( this ).next().removeClass('hide-top-left');
            }
        );
        $('.howWeWork .block.third').hover(
        function() {
                $( this ).siblings('.block.second').addClass('hide-bottom-left');
            }, function() {
                $( this ).siblings('.block.second').removeClass('hide-bottom-left');
            }
        );
    }

    //to top button
    $(function() {
        var menuBottom = $('.first-screen').offset().top + $('.first-screen').height();
        $(window).scroll(function() {
            if($(this).scrollTop() >= 50) {
                $('#toTop').fadeIn();
            } else {
            $('#toTop').fadeOut();
            }

            if ($(this).scrollTop() >= menuBottom - 5){
                $('header.top nav').addClass('fixed');
            } else{
                $('header.top nav').removeClass('fixed');
            }

            if ($(this).scrollTop() >= menuBottom){
                $('header.fixed').addClass('visible');
                $('header.top nav').addClass('visible');
            } else{
                $('header.fixed').removeClass('visible');
                $('header.top nav').removeClass('visible');
            }
        });
            $('#toTop').click(function() {
            $('body,html').animate({scrollTop:0},800);
        });
    });

    // phone mask
    $(function(){
        $("input.phone").mask("+3 8(099) 999-99-99");
    });

    //form fields validation
    $('form a.submit').click(function(event){
        event.preventDefault();

        var validName = false,
            validPhone = false,
            validEmail = false;

        var name = $('form input.name').val(),
            phone = $('form input.phone').val(),
            email = $('form input.email').val();
            post = $('form label.post input').val();
            company = $('form label.company input').val();

        $( 'form input' ).keypress(function(){
            $('form label').removeClass('error');
        });

        if( validName == false || validPhone == false || validEmail == false){
            if (!name.match(/^[A-Za-zА-Яа-яЁё\s]+$/) || !name){
                $('form label.name').addClass('error');
            } else {
                validName = true;
            }

            if (!phone){
                $('form label.phone').addClass('error');
            } else {
                validPhone = true;
            }

            if (!email.match(/^[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$/i) || email.lenght < 4 || !email){
                $('form label.email').addClass('error');
            } else {
                validEmail = true;
            }
        }

        if ( validName && validPhone && validEmail){
            var file = $('form label.file input').val().split('\\').pop();
            if (!file){
                $('form .form-thanks').addClass('visible');
                $('form input').each(function(){
                    $(this).val('');
                });
                $('form label.file').first('span').text('Выбрать файл');
            } else{
                $('form .form-error').addClass('visible');
                $('form input').each(function(){
                    $(this).val('');
                });
                $('form label.file').first('span').text('Выбрать файл');
            }
        }
    });

    //show selected filename
    $('form label.file input').on('change', function(e){
        var file = e.target.value.split('\\').pop();
        $(this).siblings('span').not('.error').text(file);
    });



    //open-close offcanvas menu
    $(document).on('click', '.mobile', function(){
        $('#offcanvas-menu').addClass('active');
        $('body').css('overflow-y', 'hidden');
    });
    $(document).on('click', '.mobile-cross', function(){
        $('#offcanvas-menu').removeClass('active');
        $('body').css('overflow-y', 'auto');

        $('.accordion > p.parent').each(function(){
            $(this).next().slideUp();
            $(this).find('i').removeClass('transform');
        });
    });


    //slideToggle offcanvas submenu
    (function($) {
        $('.accordion > .submenu').hide();
        $('.accordion > p > a').click(function() {
            if ($(this).parent().hasClass('parent')){
                $(this).parent().next().slideToggle();
                $(this).parent().siblings('p.parent').next().slideUp();
                $(this).find('i').toggleClass('transform');
                $(this).parent().siblings('p.parent').find('a i').removeClass('transform');
            }
            return false;
        });
    })(jQuery);

    $(window).resize(function(){
        if($('.first-screen .second nav .mobile').is(':visible')){
            $('.howWeWork .block.second').hover(
                function() {
                    $( this ).next().addClass('hide-top-left');
                }, function() {
                    $( this ).next().removeClass('hide-top-left');
                }
            );
            $('.howWeWork .block.third').hover(
            function() {
                    $( this ).siblings('.block.second').addClass('hide-bottom-left');
                }, function() {
                    $( this ).siblings('.block.second').removeClass('hide-bottom-left');
                }
            );
        }

        if($('.section.fillTheForm .container').css('max-width') == '570px'){
            $('.section.fillTheForm form > label > input').focus( function() {
                $(this).siblings('span.label').addClass('active');
            });

            $('.section.fillTheForm form > label > input').blur( function() {
                if (!$(this).val()){
                    $(this).siblings('span.label').removeClass('active');
                }
            });
        }
    });

    //move label on input focus
    if($('.section.fillTheForm .container').css('max-width') == '570px'){
        $('.section.fillTheForm form > label > input').focus( function() {
            $(this).siblings('span.label').addClass('active');
        });

        $('.section.fillTheForm form > label > input').blur( function() {
            if (!$(this).val()){
                $(this).siblings('span.label').removeClass('active');
            }
        });
    }


});

$(window).on('load',function(){
    $('.preloader').addClass('done');
})